package io.lenur.shop.security;

import io.lenur.shop.domain.User;

public interface Authentication {
    User login(String email, String password);
}