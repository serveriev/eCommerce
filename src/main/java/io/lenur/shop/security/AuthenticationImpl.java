package io.lenur.shop.security;

import io.lenur.di.annotation.Inject;
import io.lenur.shop.domain.User;
import io.lenur.shop.exception.AuthenticationException;
import io.lenur.shop.service.UserService;

public class AuthenticationImpl implements Authentication {
    @Inject
    private UserService userService;

    @Override
    public User login(String email, String password) {
        User user = userService.getByUserEmail(email);

        if (user == null) {
            throw new AuthenticationException("Email doesn't exist");
        }

        if (!user.getPassword().equals(password)) {
            throw new AuthenticationException("Password is incorrect");
        }

        return user;
    }
}
