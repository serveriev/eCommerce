package io.lenur.shop.security.filter;

import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.Role;
import io.lenur.shop.domain.RoleName;
import io.lenur.shop.domain.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthorizationFilter implements Filter {
    private final Map<String, Set<RoleName>> secureUrls = new HashMap<>();
    private static final Logger LOGGER = LogManager.getLogger(AuthorizationFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) {
        fillUserAccess();
        fillAdminAccess();
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String servletPath = request.getServletPath();

        if (!secureUrls.containsKey(servletPath)) {
            chain.doFilter(req, res);
            return;
        }

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Shop.SESSION_USER_ATTR);
        if (hasAccessToUrl(servletPath, user)) {
            chain.doFilter(req, res);
        } else {
            LOGGER.info("User {} wanted to get access to the {}", user.getId(), servletPath);
            RequestDispatcher requestDispatcher = request
                    .getRequestDispatcher(View.ACCESS_DENIED);
            requestDispatcher.forward(request, response);
        }
    }

    private boolean hasAccessToUrl(String servletPath, User user) {
        if (user.getRoles() == null) {
            return false;
        }

        Set<RoleName> urlRoles = secureUrls.get(servletPath);
        for (RoleName roleName: urlRoles) {
            for (Role userRole: user.getRoles()) {
                if (Objects.equals(userRole.getRoleName(), roleName)) {
                    return true;
                }
            }
        }

        return false;
    }

    private void fillUserAccess() {
        secureUrls.put("/", Set.of(RoleName.USER));
        secureUrls.put(Path.PRODUCTS, Set.of(RoleName.USER));
        secureUrls.put(Path.ORDERS, Set.of(RoleName.USER));
        secureUrls.put(Path.ORDERS_CREATE, Set.of(RoleName.USER));
        secureUrls.put(Path.ORDERS_VIEW, Set.of(RoleName.USER));
        secureUrls.put(Path.CART, Set.of(RoleName.USER));
        secureUrls.put(Path.CART_PRODUCTS, Set.of(RoleName.USER));
        secureUrls.put(Path.CART_PRODUCTS_ADD, Set.of(RoleName.USER));
        secureUrls.put(Path.CART_PRODUCTS_DELETE, Set.of(RoleName.USER));
        secureUrls.put(Path.CART_VIEW, Set.of(RoleName.USER));

        secureUrls.put(AdminPath.FILL_FIXTURES, Set.of(RoleName.USER));
    }

    private void fillAdminAccess() {
        secureUrls.put(AdminPath.ADMIN, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.ORDERS, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.ORDERS_DELETE, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.PRODUCTS, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.PRODUCTS_CREATE, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.PRODUCTS_DELETE, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.USERS, Set.of(RoleName.ADMIN));
        secureUrls.put(AdminPath.USERS_DELETE, Set.of(RoleName.ADMIN));
    }
}
