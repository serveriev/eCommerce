package io.lenur.shop.security.filter;

import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthenticationFilter implements Filter {
    private static final String SKIP_INIT_PARAM = "skipPaths";
    private List<String> skipPaths;

    @Override
    public void init(FilterConfig filterConfig) {
        String skipParam = filterConfig.getInitParameter(SKIP_INIT_PARAM);
        skipPaths = Arrays.asList(skipParam.split("\\|"));
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
        String path = request.getServletPath();

        boolean loggedIn = session != null && session.getAttribute(Shop.SESSION_USER_ATTR) != null;
        boolean canSkip = skipPaths.stream().anyMatch(path::startsWith);

        if (loggedIn || canSkip) {
            chain.doFilter(request, response);
        } else {
            String loginURI = request.getContextPath() + Path.LOGIN;
            response.sendRedirect(loginURI);
        }
    }
}
