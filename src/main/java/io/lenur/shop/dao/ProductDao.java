package io.lenur.shop.dao;

import io.lenur.shop.domain.Product;

public interface ProductDao extends Dao<Product> {
}