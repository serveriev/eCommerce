package io.lenur.shop.dao;

import io.lenur.shop.domain.User;
import java.util.Optional;

public interface UserDao extends Dao<User> {
    Optional<User> getByUserEmail(String email);
}