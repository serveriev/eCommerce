package io.lenur.shop.dao.impl.jdbc;

import io.lenur.shop.dao.ProductDao;
import io.lenur.shop.domain.Product;
import io.lenur.shop.exception.JdbcException;
import io.lenur.shop.exception.ModelNotFoundException;
import io.lenur.shop.persistence.jbdc.JdbcConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class ProductDaoImpl extends AbstractDao<Product> implements ProductDao {
    private static final String TABLE = "product";
    private static final String SQL_SELECT = String.format("SELECT * FROM `%s`", TABLE);
    private static final String SQL_INSERT = String.format("INSERT INTO `%s` (`name`, `price`) VALUES (?, ?)", TABLE);
    private static final String SQL_UPDATE = String.format("UPDATE `%s` SET `name`=?, `price`=? WHERE `id`=?", TABLE);
    private static final String SQL_DELETE = String.format("DELETE FROM `%s` WHERE `id`=?", TABLE);
    private static final String SQL_SELECT_BY_ID = String.format("SELECT * FROM `%s` WHERE `id`=?", TABLE);

    private static final Logger LOGGER = LogManager.getLogger(ProductDaoImpl.class.getName());

    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();

        try (final Connection connection = JdbcConnection.getConnection();
             final PreparedStatement statement = connection.prepareStatement(SQL_SELECT);
             final ResultSet resultSet = statement.executeQuery()
        ) {
            while (resultSet.next()) {
                Product product = mapResultSet(resultSet);
                products.add(product);
            }
        } catch (SQLException e) {
            throw new JdbcException(e.getMessage());
        }

        return products;
    }

    @Override
    public Product create(final Product product) {
        try (final Connection connection = JdbcConnection.getConnection();
             final PreparedStatement statement = connection
                     .prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)
        ) {
            statement.setString(1, product.getName());
            statement.setBigDecimal(2, product.getPrice());

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new JdbcException("Creating a product failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    Long id = generatedKeys.getLong(1);
                    product.setId(id);
                } else {
                    throw new JdbcException("Creating a product failed, no ID obtained.");
                }
            }
            LOGGER.info("The product {} was created", product.getId());
        } catch (SQLException e) {
            throw new JdbcException(e.getMessage());
        }

        return product;
    }

    @Override
    public Product update(Product product) {
        Product productPersist = get(product.getId())
                .orElseThrow(ModelNotFoundException::new);

        try (final Connection connection = JdbcConnection.getConnection();
             final PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)
        ) {
            statement.setString(1, product.getName());
            statement.setBigDecimal(2, product.getPrice());
            statement.setLong(3, productPersist.getId());
            statement.executeUpdate();
            LOGGER.info("The product {} was updated", product.getId());
        } catch (SQLException e) {
            throw new JdbcException(e.getMessage());
        }

        return product;
    }

    @Override
    public boolean delete(Long id) {
        Product product = get(id).orElseThrow(ModelNotFoundException::new);

        try (final Connection connection = JdbcConnection.getConnection();
             final PreparedStatement statement = connection.prepareStatement(SQL_DELETE)
        ) {
            statement.setLong(1, product.getId());
            boolean deleted = statement.executeUpdate() > 0;

            if (deleted) {
                LOGGER.info("The product {} was deleted", product.getId());
            }
            return deleted;
        } catch (SQLException e) {
            throw new JdbcException(e.getMessage());
        }
    }

    @Override
    protected String getOneQuery() {
        return SQL_SELECT_BY_ID;
    }

    @Override
    protected Product mapResultSet(ResultSet resultSet) throws SQLException {
        Long id = resultSet.getLong("id");
        String name = resultSet.getString("name");
        BigDecimal price = resultSet.getBigDecimal("price");

        return new Product(id, name, price);
    }
}
