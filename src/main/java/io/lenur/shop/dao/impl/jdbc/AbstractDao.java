package io.lenur.shop.dao.impl.jdbc;

import io.lenur.shop.dao.Dao;
import io.lenur.shop.domain.Identifiable;
import io.lenur.shop.exception.JdbcException;
import io.lenur.shop.persistence.jbdc.JdbcConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public abstract class AbstractDao<T extends Identifiable> implements Dao<T> {
    public Optional<T> get(Long id) {
        final String query = getOneQuery();
        try (final Connection connection = JdbcConnection.getConnection();
             final PreparedStatement statement = connection.prepareStatement(query);
        ) {
            statement.setLong(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    T object = mapResultSet(resultSet);
                    return Optional.of(object);
                }
            }
        } catch (SQLException e) {
            throw new JdbcException(e.getMessage());
        }

        return Optional.empty();
    }

    abstract protected String getOneQuery();
    abstract protected T mapResultSet(final ResultSet resultSet) throws SQLException;
}
