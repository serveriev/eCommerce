package io.lenur.shop.dao.impl.memory;

import io.lenur.shop.dao.UserDao;
import io.lenur.shop.domain.User;
import io.lenur.shop.persistence.memory.Storage;

import java.util.List;
import java.util.Optional;

public class UserDaoImpl extends AbstractDao<User> implements UserDao {

    @Override
    public Optional<User> getByUserEmail(String email) {
        return getAll()
                .stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst();
    }

    @Override
    public List<User> getAll() {
        return Storage.getUsers();
    }

    @Override
    public User create(User user) {
        Storage.addUser(user);

        return user;
    }

    @Override
    public User update(User user) {
        int index = getIndex(user);
        List<User> users = getAll();

        users.set(index, user);

        return user;
    }

    @Override
    public boolean delete(Long id) {
        List<User> users = getAll();

        return users.removeIf(x -> x.getId().equals(id));
    }
}
