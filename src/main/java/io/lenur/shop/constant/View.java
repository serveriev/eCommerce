package io.lenur.shop.constant;

public class View extends BaseView {
    public static final String SECURITY = JSP + "/security";
    public static final String REGISTRATION_FORM = SECURITY + "/registration.jsp";
    public static final String LOGIN_FORM = SECURITY + "/login.jsp";
    public static final String ACCESS_DENIED = SECURITY + "/access_denied.jsp";

    public static final String HOME = JSP + "/home.jsp";

    public static final String PRODUCT = JSP + "/product";
    public static final String PRODUCT_LIST = PRODUCT + "/list.jsp";

    public static final String ORDER = JSP + "/order";
    public static final String ORDER_LIST = ORDER + "/list.jsp";
    public static final String ORDER_VIEW = ORDER + "/view.jsp";

    public static final String CART = JSP + "/cart";
    public static final String CART_VIEW = CART + "/view.jsp";
}
