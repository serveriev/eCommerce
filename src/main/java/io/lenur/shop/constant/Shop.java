package io.lenur.shop.constant;

public class Shop {
    public static final String PACKAGE_NAME = "io.lenur.shop";

    public static final String SESSION_USER_ATTR = "user";
}
