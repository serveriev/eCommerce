package io.lenur.shop.constant;

public class AdminPath {
    public static final String ADMIN = "/admin";
    public static final String FILL_FIXTURES = ADMIN + "/fill-fixtures";

    public static final String PRODUCTS = ADMIN + "/products";
    public static final String PRODUCTS_CREATE = PRODUCTS + "/create";
    public static final String PRODUCTS_DELETE = PRODUCTS + "/delete";

    public static final String ORDERS = ADMIN + "/orders";
    public static final String ORDERS_DELETE = ORDERS + "/delete";

    public static final String USERS = ADMIN + "/users";
    public static final String USERS_DELETE = USERS + "/delete";
}
