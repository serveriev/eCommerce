package io.lenur.shop.constant;

public class AdminView extends BaseView {
    public static final String ADMIN = JSP + "/admin";

    public static final String PRODUCT = ADMIN + "/product";
    public static final String PRODUCT_LIST = PRODUCT + "/list.jsp";
    public static final String PRODUCT_CREATE = PRODUCT + "/create.jsp";

    public static final String ORDER = ADMIN + "/order";
    public static final String ORDER_LIST = ORDER + "/list.jsp";

    public static final String USER = ADMIN + "/user";
    public static final String USER_LIST = USER + "/list.jsp";
}
