package io.lenur.shop.constant;

public class Path {
    public static final String REGISTRATION = "/registration";
    public static final String LOGIN = "/login";
    public static final String LOGOUT = "/logout";

    public static final String PRODUCTS = "/products";

    public static final String ORDERS = "/orders";
    public static final String ORDERS_CREATE = ORDERS + "/create";
    public static final String ORDERS_VIEW = ORDERS + "/view";

    public static final String CART = "/cart";
    public static final String CART_VIEW = CART + "/view";
    public static final String CART_PRODUCTS = CART + "/products";
    public static final String CART_PRODUCTS_ADD = CART_PRODUCTS + "/add";
    public static final String CART_PRODUCTS_DELETE = CART_PRODUCTS + "/delete";
}
