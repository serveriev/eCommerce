package io.lenur.shop.dependency;

import io.lenur.di.annotation.Dependencies;
import io.lenur.di.annotation.Instance;
import io.lenur.shop.security.Authentication;
import io.lenur.shop.security.AuthenticationImpl;

@Dependencies
public class Security {
    @Instance
    public Authentication getAuthenticationService() {
        return new AuthenticationImpl();
    }
}
