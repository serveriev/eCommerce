package io.lenur.shop.dependency;

import io.lenur.di.annotation.Dependencies;
import io.lenur.di.annotation.Instance;
import io.lenur.shop.dao.CartDao;
import io.lenur.shop.dao.OrderDao;
import io.lenur.shop.dao.UserDao;
import io.lenur.shop.dao.impl.jdbc.ProductDaoImpl;
import io.lenur.shop.dao.ProductDao;
import io.lenur.shop.dao.impl.memory.CartDaoImpl;
import io.lenur.shop.dao.impl.memory.OrderDaoImpl;
import io.lenur.shop.dao.impl.memory.UserDaoImpl;

@Dependencies
public class Dao {
    @Instance
    public ProductDao getProductDao() {
        return new ProductDaoImpl();
    }

    @Instance
    public UserDao getUserDao() {
        return new UserDaoImpl();
    }

    @Instance
    public CartDao getCartDao() {
        return new CartDaoImpl();
    }

    @Instance
    public OrderDao getOrderDao() {
        return new OrderDaoImpl();
    }
}
