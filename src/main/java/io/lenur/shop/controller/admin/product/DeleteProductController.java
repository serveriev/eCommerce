package io.lenur.shop.controller.admin.product;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.service.ProductService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.PRODUCTS_DELETE)
public class DeleteProductController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final ProductService productService = (ProductService) packageContext
            .getInstance(ProductService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String productId = request.getParameter("id");
        Long id = Long.parseLong(productId);

        productService.delete(id);

        response.sendRedirect(request.getContextPath() + AdminPath.PRODUCTS);
    }
}
