package io.lenur.shop.controller.admin.order;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.service.OrderService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.ORDERS_DELETE)
public class DeleteOrderController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final OrderService orderService = (OrderService) packageContext
            .getInstance(OrderService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String orderId = request.getParameter("id");
        Long id = Long.parseLong(orderId);

        orderService.delete(id);

        response.sendRedirect(request.getContextPath() + AdminPath.ORDERS);
    }
}
