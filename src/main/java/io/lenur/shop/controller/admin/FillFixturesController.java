package io.lenur.shop.controller.admin;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.domain.Product;
import io.lenur.shop.domain.Role;
import io.lenur.shop.domain.RoleName;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.ProductService;
import io.lenur.shop.service.UserService;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Set;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.FILL_FIXTURES)
public class FillFixturesController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final UserService userService = (UserService) packageContext
            .getInstance(UserService.class);

    private final ProductService productService = (ProductService) packageContext
            .getInstance(ProductService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        this.createUsers();
        this.createProducts();

        response.sendRedirect(request.getContextPath() + "/");
    }

    private void createUsers() {
        User user = new User("user 1", "user1@example.com", "password");
        user.setRoles(Set.of(Role.of(RoleName.USER)));

        User admin = new User("admin", "admin@example.com", "password");
        admin.setRoles(Set.of(Role.of(RoleName.USER), Role.of(RoleName.ADMIN)));

        userService.create(user);
        userService.create(admin);
    }

    private void createProducts() {
        Product product1 = new Product("product 1", BigDecimal.valueOf(10.02));
        Product product2 = new Product("product 2", BigDecimal.valueOf(20.03));

        productService.create(product1);
        productService.create(product2);
    }
}
