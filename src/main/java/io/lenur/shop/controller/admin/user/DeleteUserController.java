package io.lenur.shop.controller.admin.user;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.service.UserService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.USERS_DELETE)
public class DeleteUserController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final UserService userService = (UserService) packageContext
            .getInstance(UserService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String userId = request.getParameter("id");
        Long id = Long.parseLong(userId);

        userService.delete(id);

        response.sendRedirect(request.getContextPath() + AdminPath.USERS);
    }
}
