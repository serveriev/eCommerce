package io.lenur.shop.controller.admin.product;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.AdminView;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.domain.Product;
import io.lenur.shop.service.ProductService;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.PRODUCTS)
public class ProductsController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final ProductService productService = (ProductService) packageContext
            .getInstance(ProductService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Product> products = productService.getAll();
        request.setAttribute("products", products);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(AdminView.PRODUCT_LIST);
        requestDispatcher.forward(request, response);
    }
}
