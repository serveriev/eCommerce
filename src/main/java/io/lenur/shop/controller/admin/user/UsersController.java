package io.lenur.shop.controller.admin.user;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.AdminView;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.UserService;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.USERS)
public class UsersController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final UserService userService = (UserService) packageContext
            .getInstance(UserService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<User> users = userService.getAll();

        request.setAttribute("users", users);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(AdminView.USER_LIST);
        requestDispatcher.forward(request, response);
    }
}
