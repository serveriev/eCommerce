package io.lenur.shop.controller.admin.product;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.AdminView;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.domain.Product;
import io.lenur.shop.service.ProductService;

import java.io.IOException;
import java.math.BigDecimal;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.PRODUCTS_CREATE)
public class CreateProductController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final ProductService productService = (ProductService) packageContext
            .getInstance(ProductService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(AdminView.PRODUCT_CREATE);
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String name = request.getParameter("name");
        String price = request.getParameter("price");
        BigDecimal priceParsed = new BigDecimal(price);

        Product product = new Product(name, priceParsed);
        productService.create(product);

        response.sendRedirect(request.getContextPath() + AdminPath.PRODUCTS);
    }
}
