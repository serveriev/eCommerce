package io.lenur.shop.controller.admin.order;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.AdminPath;
import io.lenur.shop.constant.AdminView;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.domain.Order;
import io.lenur.shop.service.OrderService;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = AdminPath.ORDERS)
public class OrdersController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final OrderService orderService = (OrderService) packageContext
            .getInstance(OrderService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Order> orders = orderService.getAll();

        request.setAttribute("orders", orders);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(AdminView.ORDER_LIST);
        requestDispatcher.forward(request, response);
    }
}
