package io.lenur.shop.controller.security;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.User;
import io.lenur.shop.exception.AuthenticationException;
import io.lenur.shop.security.Authentication;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = Path.LOGIN)
public class LoginController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final Authentication authenticationService = (Authentication) packageContext
            .getInstance(Authentication.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(View.LOGIN_FORM);
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        try {
            User user = authenticationService.login(email, password);
            HttpSession session = request.getSession();
            session.setAttribute(Shop.SESSION_USER_ATTR, user);
            response.sendRedirect(request.getContextPath() + "/");
        } catch (AuthenticationException e) {
            request.setAttribute("errorMessage", e.getMessage());
            request.getRequestDispatcher(View.LOGIN_FORM).forward(request, response);
        }
    }
}
