package io.lenur.shop.controller.security;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.Role;
import io.lenur.shop.domain.RoleName;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.UserService;

import java.io.IOException;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = Path.REGISTRATION)
public class RegistrationController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final UserService userService = (UserService) packageContext
            .getInstance(UserService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(View.REGISTRATION_FORM);
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordRepeat = request.getParameter("password-repeat");

        if (password.equals(passwordRepeat)) {
            User user = new User(name, email, password);
            user.setRoles(Set.of(Role.of(RoleName.USER)));
            userService.create(user);
            response.sendRedirect(request.getContextPath());
        } else {
            String errorMessage = "The password and repeat password are mismatch";
            request.setAttribute("name", name);
            request.setAttribute("email", email);
            request.setAttribute("errorMessage", errorMessage);
            request.getRequestDispatcher(View.REGISTRATION_FORM).forward(request, response);
        }
    }
}
