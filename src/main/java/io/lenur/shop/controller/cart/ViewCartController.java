package io.lenur.shop.controller.cart;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.Cart;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.CartService;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = Path.CART_VIEW)
public class ViewCartController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final CartService cartService = (CartService) packageContext
            .getInstance(CartService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Shop.SESSION_USER_ATTR);
        Cart cart = cartService.getByUserId(user.getId());

        request.setAttribute("products", cart.getProducts());
        request.setAttribute("user", user);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(View.CART_VIEW);
        requestDispatcher.forward(request, response);
    }
}
