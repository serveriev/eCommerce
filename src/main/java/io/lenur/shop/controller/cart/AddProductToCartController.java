package io.lenur.shop.controller.cart;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.domain.Cart;
import io.lenur.shop.domain.Product;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.CartService;
import io.lenur.shop.service.ProductService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = Path.CART_PRODUCTS_ADD)
public class AddProductToCartController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final CartService cartService = (CartService) packageContext
            .getInstance(CartService.class);

    private final ProductService productService = (ProductService) packageContext
            .getInstance(ProductService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Shop.SESSION_USER_ATTR);

        String productId = request.getParameter("id");
        Long id = Long.parseLong(productId);

        Product product = productService.get(id);
        Cart cart = cartService.getByUserId(user.getId());
        cartService.addProduct(cart, product);

        response.sendRedirect(request.getContextPath() + Path.PRODUCTS);
    }
}
