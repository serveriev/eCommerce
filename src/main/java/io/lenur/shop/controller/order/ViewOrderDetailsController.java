package io.lenur.shop.controller.order;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.Order;
import io.lenur.shop.service.OrderService;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = Path.ORDERS_VIEW)
public class ViewOrderDetailsController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final OrderService orderService = (OrderService) packageContext
            .getInstance(OrderService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String orderId = request.getParameter("id");
        Long id = Long.parseLong(orderId);

        Order order = orderService.get(id);

        request.setAttribute("order", order);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(View.ORDER_VIEW);
        requestDispatcher.forward(request, response);
    }
}
