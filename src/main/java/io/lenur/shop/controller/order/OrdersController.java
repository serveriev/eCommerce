package io.lenur.shop.controller.order;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.Order;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.OrderService;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = Path.ORDERS)
public class OrdersController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final OrderService orderService = (OrderService) packageContext
            .getInstance(OrderService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Shop.SESSION_USER_ATTR);

        List<Order> orders = orderService.getUserOrders(user.getId());

        request.setAttribute("orders", orders);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(View.ORDER_LIST);
        requestDispatcher.forward(request, response);
    }
}
