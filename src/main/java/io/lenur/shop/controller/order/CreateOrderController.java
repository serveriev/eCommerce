package io.lenur.shop.controller.order;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.domain.Cart;
import io.lenur.shop.domain.User;
import io.lenur.shop.service.CartService;
import io.lenur.shop.service.OrderService;
import io.lenur.shop.service.UserService;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(value = Path.ORDERS_CREATE)
public class CreateOrderController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final CartService cartService = (CartService) packageContext
            .getInstance(CartService.class);

    private final OrderService orderService = (OrderService) packageContext
            .getInstance(OrderService.class);

    private final UserService userService = (UserService) packageContext
            .getInstance(UserService.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute(Shop.SESSION_USER_ATTR);
        Cart cart = cartService.getByUserId(user.getId());

        orderService.completeOrder(cart, user);
        response.sendRedirect(request.getContextPath() + Path.ORDERS);
    }
}
