package io.lenur.shop.controller.product;

import io.lenur.di.Dependency;
import io.lenur.di.PackageContext;
import io.lenur.shop.constant.Path;
import io.lenur.shop.constant.Shop;
import io.lenur.shop.constant.View;
import io.lenur.shop.domain.Product;
import io.lenur.shop.service.ProductService;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(value = Path.PRODUCTS)
public class ProductsController extends HttpServlet {
    private static final PackageContext packageContext = Dependency
            .init(Shop.PACKAGE_NAME);

    private final ProductService productService = (ProductService) packageContext
            .getInstance(ProductService.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Product> products = productService.getAll();
        request.setAttribute("products", products);

        RequestDispatcher requestDispatcher = request
                .getRequestDispatcher(View.PRODUCT_LIST);
        requestDispatcher.forward(request, response);
    }
}
