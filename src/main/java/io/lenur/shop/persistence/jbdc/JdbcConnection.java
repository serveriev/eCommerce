package io.lenur.shop.persistence.jbdc;

import io.lenur.configuration.Configuration;
import io.lenur.shop.exception.JdbcException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcConnection {
    private static final Configuration configuration = new Configuration("application.properties");

    static {
        try {
            Class.forName(configuration.get("database.driver-class-name"));
        } catch (ClassNotFoundException e) {
            throw new JdbcException(e.getMessage());
        }
    }

    public static Connection getConnection() {
        Properties properties = new Properties();
        properties.put("user", configuration.get("database.username"));
        properties.put("password", configuration.get("database.password"));

        try {
            final String url = configuration.get("database.url");
            return DriverManager.getConnection(url, properties);
        } catch (SQLException e) {
            throw new JdbcException(e.getMessage());
        }
    }
}