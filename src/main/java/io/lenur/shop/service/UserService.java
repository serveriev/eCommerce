package io.lenur.shop.service;

import io.lenur.shop.domain.User;

public interface UserService extends Service<User> {
    User getByUserEmail(String email);
}