package io.lenur.shop.service;

import io.lenur.shop.domain.Product;

public interface ProductService extends Service<Product> {
}