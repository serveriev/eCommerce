package io.lenur.shop.domain;

public enum RoleName {
    USER,
    ADMIN
}
