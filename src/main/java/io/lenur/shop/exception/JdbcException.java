package io.lenur.shop.exception;

public class JdbcException extends RuntimeException {
    public JdbcException(String message) {
        super(message);
    }
}
