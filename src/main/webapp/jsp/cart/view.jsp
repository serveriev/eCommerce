<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="title">Cart</jsp:attribute>
    <jsp:attribute name="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Cart</li>
        </ol>
    </jsp:attribute>
    <jsp:body>
        <h1>Hi ${user.name}. Here is your shopping cart</h1>
        <c:if test="${products.size() > 0}">
            <table class="table">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Action</th>
                </tr>
                <c:forEach var="product" items="${products}">
                    <tr>
                        <td><c:out value="${product.id}"/></td>
                        <td><c:out value="${product.name}"/></td>
                        <td><c:out value="${product.price}"/></td>
                        <td><a href="${pageContext.request.contextPath}/cart/products/delete?id=${product.id}">Delete</a></td>
                    </tr>
                </c:forEach>
            </table>
            <form method="POST" action="${pageContext.request.contextPath}/orders/create">
                <button type="submit" class="btn btn-primary">Complete order</button>
            </form>
        </c:if>
    </jsp:body>
</t:layout>