<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layoutSecurity>
    <jsp:attribute name="title">Login</jsp:attribute>
    <jsp:body>
        <form class="form-security" action="${pageContext.request.contextPath}/login" method="POST">
            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
            <span class="badge badge-pill badge-danger">${errorMessage}</span>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
            <div class="checkbox mb-3">
                <a href="${pageContext.request.contextPath}/registration">Register,</a> if you do not have an account.
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
    </jsp:body>
</t:layoutSecurity>
