<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layoutSecurity>
    <jsp:attribute name="title">Registration</jsp:attribute>
    <jsp:body>
        <form class="form-security" action="${pageContext.request.contextPath}/registration" method="POST">
           <h1 class="h3 mb-3 font-weight-normal">Registration</h1>
            <span class="badge badge-pill badge-danger">${errorMessage}</span>
            <label for="name" class="sr-only">Name</label>
            <input type="text" id="name" class="form-control" placeholder="Enter name" name="name" value="${name}" required autofocus>
            <label for="email" class="sr-only">Email address</label>
            <input type="email" class="form-control" name="email" id="email" value="${email}" placeholder="Enter email" required>
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
            <label for="password-repeat" class="sr-only">Repeat password</label>
            <input type="password" id="password-repeat" name="password-repeat" class="form-control" placeholder="Repeat password" required>
            <div class="checkbox mb-3">
                <a href="${pageContext.request.contextPath}/login">Login,</a> if you have an account.
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        </form>
    </jsp:body>
</t:layoutSecurity>
