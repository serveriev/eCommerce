<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layoutSecurity>
    <jsp:attribute name="title">Access denied</jsp:attribute>
    <jsp:body>
        <h1 class="badge-danger">You do not have access to this resource!</h1>
    </jsp:body>
</t:layoutSecurity>
