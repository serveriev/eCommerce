<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="title">Order details</jsp:attribute>
    <jsp:attribute name="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}/orders">Orders</a></li>
            <li class="breadcrumb-item active" aria-current="page">Order details</li>
        </ol>
    </jsp:attribute>
    <jsp:body>
        <h1>Order details</h1>
        <c:if test="${order != null}">
            <div class="row">
                <div class="col-sm">
                    <h2>Products</h2>
                    <table class="table">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                        </tr>
                        <c:forEach var="product" items="${order.products}">
                            <tr>
                                <td><c:out value="${product.id}"/></td>
                                <td><c:out value="${product.name}"/></td>
                                <td><c:out value="${product.price}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="col-sm">
                    <h2>User</h2>
                    <table class="table">
                        <tr>
                            <th>Id: </th>
                            <td>${order.user.id}</td>
                        </tr>
                        <tr>
                            <th>Name: </th>
                            <td>${order.user.name}</td>
                        </tr>
                        <tr>
                            <th>Email: </th>
                            <td>${order.user.email}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </c:if>
    </jsp:body>
</t:layout>