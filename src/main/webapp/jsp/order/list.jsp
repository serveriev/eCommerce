<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="title">Orders</jsp:attribute>
    <jsp:attribute name="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Orders</li>
        </ol>
    </jsp:attribute>
    <jsp:body>
        <h1>Orders</h1>
        <c:if test="${orders.size() > 0}">
            <table class="table">
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Count of products</th>
                    <th scope="col">View details</th>
                </tr>
                <c:forEach var="order" items="${orders}">
                    <tr>
                        <td><c:out value="${order.id}"/></td>
                        <td><c:out value="${order.products.size()}"/></td>
                        <td><a href="${pageContext.request.contextPath}/orders/view?id=${order.id}">View an order details</a></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </jsp:body>
</t:layout>