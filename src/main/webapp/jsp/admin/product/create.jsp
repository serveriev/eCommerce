<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="title">Admin / Create a new product</jsp:attribute>
    <jsp:attribute name="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Admin / Create a new product</li>
        </ol>
    </jsp:attribute>
    <jsp:body>
        <h1>Admin -> Create a new product</h1>
        <form action="${pageContext.request.contextPath}/admin/products/create" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" class="form-control" id="price" name="price">
            </div>
            <div>
                <button class="btn btn-primary" type="submit">Add</button>
            </div>
        </form>
    </jsp:body>
</t:layout>