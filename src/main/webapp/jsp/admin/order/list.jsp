<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:layout>
    <jsp:attribute name="title">Admin / Orders</jsp:attribute>
    <jsp:attribute name="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.request.contextPath}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Admin / Orders</li>
        </ol>
    </jsp:attribute>
    <jsp:body>
        <h1>Admin / Orders</h1>
        <c:if test="${orders.size() > 0}">
            <table class="table">
                <tr>
                    <th>Id</th>
                    <th>Products</th>
                    <th>User</th>
                    <th>Action</th>
                </tr>
                <c:forEach var="order" items="${orders}">
                    <tr>
                        <td><c:out value="${order.id}"/></td>
                        <td>
                            <table class="table table-nested">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="product" items="${order.products}">
                                        <tr>
                                            <td><c:out value="${product.id}"/></td>
                                            <td><c:out value="${product.name}"/></td>
                                            <td><c:out value="${product.price}"/></td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="table table-nested">
                                <thead>
                                    <tr>
                                        <th>Id: </th>
                                        <td>${order.user.id}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Name: </th>
                                        <td>${order.user.name}</td>
                                    </tr>
                                    <tr>
                                        <th>Email: </th>
                                        <td>${order.user.email}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td><a href="${pageContext.request.contextPath}/admin/orders/delete?id=${order.id}">Delete</a></td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </jsp:body>
</t:layout>