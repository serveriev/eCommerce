<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Secutity layout" pageEncoding="UTF-8" %>
<%@ attribute name="title" fragment="true" %>
<html>
    <head>
        <title>
            <jsp:invoke fragment="title"/>
        </title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/security.css" />
    </head>
    <body class="text-center">
        <jsp:doBody/>
    </body>
</html>