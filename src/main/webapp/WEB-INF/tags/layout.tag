<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag description="Layout" pageEncoding="UTF-8" %>
<%@ attribute name="title" fragment="true" %>
<%@ attribute name="breadcrumb" fragment="true" %>
<html>
    <head>
        <title>
            <jsp:invoke fragment="title"/>
        </title>
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.min.css" />
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/common.css" />
    </head>
    <body>
        <nav class="site-header py-1">
            <div class="container d-flex flex-column flex-md-row justify-content-between">
                <a class="py-2 d-none d-md-inline-block" href="${pageContext.request.contextPath}">Home</a>
                <a class="py-2 d-none d-md-inline-block" href="${pageContext.request.contextPath}/registration">Registration</a>
                <a class="py-2 d-none d-md-inline-block" href="${pageContext.request.contextPath}/products">Products</a>
                <a class="py-2 d-none d-md-inline-block" href="${pageContext.request.contextPath}/cart/view">View cart</a>
                <a class="py-2 d-none d-md-inline-block" href="${pageContext.request.contextPath}/orders">Orders</a>
                <a class="py-2 d-none d-md-inline-block" href="${pageContext.request.contextPath}/logout">Logout</a>
                <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin</a>
                <div class="dropdown-menu" aria-labelledby="dropdown02">
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/admin/fill-fixtures">Fill fixtures</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/admin/users">Users</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/admin/orders">Orders</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/admin/products">Products</a>
                    <a class="dropdown-item" href="${pageContext.request.contextPath}/admin/products/create">Create a product</a>
                </div>
            </div>
        </nav>
        <main role="main">
            <div class="container">
                <nav aria-label="breadcrumb">
                    <jsp:invoke fragment="breadcrumb"/>
                </nav>
                <jsp:doBody/>
            </div>
        </main>
        <script src="${pageContext.request.contextPath}/assets/js/jquery-3.5.1.slim.min.js"></script>
        <script src="${pageContext.request.contextPath}/assets/js/bootstrap.bundle.min.js"></script>
    </body>
</html>