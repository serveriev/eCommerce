#!/bin/sh

export APP_SHOP_PATH={path_to_shop}
export APP_SHOP_DB_HOST={db_host}
export APP_SHOP_DB_PORT={db_port}
export APP_SHOP_DB_NAME={db_name}
export APP_SHOP_DB_USER={db_user}
export APP_SHOP_DB_PASSWORD={db_password}
