# eCommerce implementation in java

##Setting the env variables. 

If you use tomcat, you can copy **setenv.sh** to the bin folder of tomcat:

```sh
cp setenv.sh {tomcat_folder}/bin/
sudo chmod +x setenv.sh
```
And restart tomcat.